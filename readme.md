# DeltaRune Dumper
DeltaRune Dumper is a tool that reads the process memory of DeltaRune and, using the built-in GameMaker engine functions, reads and prints out map information and global variable information of the game.

[More information](https://www.unknowncheats.me/forum/other-games/307281-deltarune-dumper.html)

# Usage
1. Download the module.
2. Download a remote library injector 
3. Run DeltaRune
4. Inject the module into the DeltaRune process

After completing the above steps, there should now be 2 new text files in the directory of the DeltaRune Dumper DLL - "rooms.txt" and "globals.txt" which contain information about all of the existing rooms in the game and all of the global variables respectively.

# License
This project is licensed under the MIT license