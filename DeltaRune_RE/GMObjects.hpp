#pragma once
#include <string>

typedef void * PVOID;
typedef unsigned long DWORD;

enum GMVarType
{
	Double,
	String,
	Array,
	Pointer,
	ObjPointer = 6,
	Integer,
	Boolean = 13
};

class GMVar
{
public:
	DWORD pValue; // 0
	DWORD Unk1; // 4
	DWORD Unk2; // 8
	GMVarType Type; // C

	std::string ValueAsString()
	{
		char Buffer[256];

		if (Type == 0xFFFFFF)
			return "Variable not set";

		//if (!IsValid())
			//return std::string("Invalid variable");

		if (Type == GMVarType::Double)
		{
			double dVal = *reinterpret_cast<double*>(&pValue);
			float fVal = static_cast<float>(dVal);
			sprintf_s(Buffer, "%.3f", fVal);
			return std::string(Buffer);
		}
		if (Type == GMVarType::String)
		{
			char* szVal = *reinterpret_cast<char**>(pValue);
			return std::string(szVal);
		}
		if (Type == GMVarType::Array)
		{
			DWORD *pArrayStart = reinterpret_cast<DWORD*>(pValue + 0x4);
			DWORD ArrayStart = *pArrayStart;
			ArrayStart = *reinterpret_cast<DWORD*>(ArrayStart + 0x4);
			int ElementCount = *reinterpret_cast<int*>(*pArrayStart);

			std::string RetVal = "{";
			for (int i = 0; i < ElementCount; ++i) {
				GMVar *pVar = reinterpret_cast<GMVar*>(ArrayStart + i * 0x10);
				auto szValue = pVar->ValueAsString();
				RetVal += szValue + (i == ElementCount - 1 ? " }" : " | ");
			}
			return RetVal;
		}
		if (Type == GMVarType::Integer)
		{
			int iVal = *reinterpret_cast<int*>(&pValue);
			sprintf_s(Buffer, "%d", iVal);
			return std::string(Buffer);
		}
		return "????";
	}

	std::string TypeAsString()
	{
		if (Type > 0xE)
			return "INVALID";

		switch (Type)
		{
		case Double:
			return "Double";
		case String:
			return "String";
		case Array:
			return "Array";
		case Pointer:
			return "Pointer";
		case ObjPointer:
			return "ObjectPointer";
		case Integer:
			return "Integer";
		case Boolean:
			return "Boolean";
		default:
			return "INVALID";
		}
	}

	bool IsValid()
	{
		if (pValue == 0 || Type == 0xFFFFFF || Type > 0xE)
			return false;
		return true;
	}
};

