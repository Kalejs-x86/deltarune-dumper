#pragma once
#include <windef.h>

namespace Address
{
	// Functions
	const DWORD Func_SwitchRoom = 0x4FECD0;
	const DWORD Func_GetRoomName = 0x4B5DA0;
	const DWORD Func_GetGlobalVarName = 0x56F4A0;
	const DWORD Func_GetGlobalVarPtr = 0x494D00;

	// Globals
	const DWORD MaxRooms = 0x89C3D8;
	const DWORD MemoryManager = 0x89C3E0;
	const DWORD GameInstance = 0xAB9690;
}