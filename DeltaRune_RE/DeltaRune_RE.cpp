#include <Windows.h>
#include <thread>
#include <fstream>
#include <process.h>

#include "Addresses.hpp"
#include "GMObjects.hpp"

typedef unsigned long long QWORD;

// Functions prefixed by "Delta_" are engine/host process functions which are called from this module
typedef void(__cdecl *SwitchRoomFn)(int RoomIndex);
SwitchRoomFn Delta_SwitchRoom = nullptr;
typedef char *(__cdecl *GetRoomNameFn)(int RoomIndex);
GetRoomNameFn Delta_GetRoomName = nullptr;
typedef char *(__cdecl *GetGlobalVarNameFn)(void *BasePtr, DWORD Unk, DWORD VarIndex);
GetGlobalVarNameFn Delta_GetGlobalVarName = nullptr;
typedef GMVar *(__thiscall *GetGlobalVarPtrFn)(void *pThis, DWORD RealVarIndex); // MemoryManager::
GetGlobalVarPtrFn Delta_GetGlobalVarPtr = nullptr;


void *pGameInstance = nullptr;
void *pMemoryManager = nullptr;


std::string ExtendText(std::string Text, int MinimumLength)
{
	std::string Temp(Text);
	if (Text.length() < MinimumLength)
	{
		for (int i = 0; i < MinimumLength - Text.length(); ++i)
			Temp += " ";
	}
	return Temp;
}

void DumpRooms(std::string Path)
{
	Path += "rooms.txt";

	std::ofstream os(Path);
	if (os.is_open())
	{
		os << "DELTARUNE Room Dump by Kalejs\n\n";
		os << "Index\tRoom Identifier\n";
		os << "=================================\n";

		int *MaxRooms = reinterpret_cast<int*>(Address::MaxRooms);
		for (int i = 0; i < *MaxRooms; ++i)
		{
			char *szRoomName = Delta_GetRoomName(i);
			os << i << "\t\t" << szRoomName << "\n";
		}
	}
	os.close();
}

void DumpGlobals(std::string Path)
{
	Path += "globals.txt";

	std::ofstream os(Path);
	if (os.is_open())
	{
		os << "DELTARUNE Variable Dump by Kalejs\n\n";
		os << "Index\t\tVariable name\t\t\t\tAddress\t\tType\tValue\n";
		os << "===================================================================\n";
		
		// Unknown variable after 1906C
		for (DWORD i = 0x186A0; i <= 0x1906C; ++i)
		{
			// The bitmask (i assume), 0xFFFFFFFB, I just copied from the game's code, since that's
			// what was used for every call to this function
			char *szName = Delta_GetGlobalVarName(*(void**)((DWORD)pGameInstance + 0x34), 0xFFFFFFFB, i);
			if (szName == nullptr)
				continue;
			
			GMVar *pVar = Delta_GetGlobalVarPtr(pMemoryManager, i - 0x186A0);

			char Buffer[2048];
			std::string szValue = pVar->ValueAsString();
			std::string szType = pVar->TypeAsString();
			snprintf(Buffer, 2048, "%d[%X]\t%s\t\t%X\t\t%s\t%s", i - 0x186A0, i, ExtendText(szName, 20).c_str(), pVar, szType.c_str(), szValue.c_str());
			os << "\n"; // Otherwise newline might get truncated by snprintf
			os << Buffer;
				
		}
	}
	os.close();
}

HINSTANCE hDLL;
void MainThread()
{
	while (pGameInstance == nullptr){
		pGameInstance = *(void**)(Address::GameInstance);
	}

	while (pMemoryManager == nullptr) {
		pMemoryManager = *(void**)(Address::MemoryManager);
	}

	char Buffer[256];
	GetModuleFileName(hDLL, Buffer, 256);
	std::string szModulePath(Buffer);
	int LastSlash = szModulePath.find_last_of("\\");
	szModulePath = szModulePath.substr(0, LastSlash+1);

	DumpRooms(szModulePath);
	DumpGlobals(szModulePath);
	MessageBox(0, "Dump complete\nInformation written to TXTs in DLL directory", "DELTARUNE Dumper by Kalejs", MB_OK);
}


BOOL WINAPI DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD     fdwReason,
	_In_ LPVOID    lpvReserved)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		hDLL = hinstDLL;
		Delta_SwitchRoom = (SwitchRoomFn)Address::Func_SwitchRoom;
		Delta_GetRoomName = (GetRoomNameFn)Address::Func_GetRoomName;
		Delta_GetGlobalVarName = (GetGlobalVarNameFn)Address::Func_GetGlobalVarName;
		Delta_GetGlobalVarPtr = (GetGlobalVarPtrFn)Address::Func_GetGlobalVarPtr;

		_beginthread((_beginthread_proc_type)MainThread, 0, NULL);
		break;
	}
}